///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';


double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}


double fromMegatonToJoule( double megaton ) {
   return -1.0;  /// @todo Stubbed out
}

double fromJouleToMegaton( double joule ) {
   return -1.0;  /// @todo Stubbed out
}

/// @todo Add other functions here

double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}




int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

   printf( "fromValue = [%lG]\n", fromValue );   /// @todo Remove before flight
   printf( "fromUnit = [%c]\n", fromUnit );      /// @todo Remove before flight
   printf( "toUnit = [%c]\n", toUnit );          /// @todo Remove before flight

   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
   }

   printf( "commonValue = [%lG] joule\n", commonValue ); /// @todo Remove before flight


	/// @todo Write a switch statement to convert common units to toUnit.  
	///       Don't forget to break; after every case.
   double toValue;


   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}
